<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="styles.css" />        
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>  
        <script type="text/javascript" src="scripts.js"></script>
        <?php 
        require_once("dbClass.php");         
        $db = db::getInstance();
        $table = "menu";        
       
        // insert data from form
        if(isset($_POST['addMenuItem'])){

                $name = $_POST['menuName'];
                $parent = 0;
                if(isset($_POST['parent'])){
                    $parent = $_POST['parent'];
                }            
                $data = array(               
                    'name'=>$name,
                    'parent'=>$parent
                ); 
                $send = $_POST['addMenuItem'];      
            
                if($send==TRUE){                 
                        $insert = $db->insert($table, $data);   
                       echo  "New Menu Item Was Added !!!";
                } 
         }
         
        // edit  data
        if(isset($_POST['editMenuItem'])){

                $name = $_POST['menuName'];
                $menuId = $_POST['menuId'];
                $parent = 0;
                if(isset($_POST['parent'])){
                $parent = $_POST['parent'];
          }
                $id = $_POST['menuId'];
                $data = array(
                        'name'=>$name,
                        'parent'=>$parent
                 ); 
         $send = $_POST['editMenuItem'];      

        if($send==TRUE){                   
              $where = 'id = '.$id;
              $insert = $db->update($table, $data,$where);   
                      echo  "Menu Item Was Updated !!!";
              } 
        }
        
        //delete item by id 
        if(isset($_GET['del'])){
               
                $del_id = $_GET['del'];
                $where = 'id = '.$del_id;
              
                $db->delete($table,$where) ;
        }
         
       ?>
    </head>
    <body>
        <div class="body_">
            <div class="topMenu group">
                <ul>  
                    <li><a href="index.php">Home</a></li>
                   <li><a href="index.php?p=addMenu">Add Menu</a></li>
                   <li><a href="index.php?p=showTree">Show Tree</a></li>
                   <li><a href="index.php?p=editTree">Edit List </a></li>

                </ul>
            </div>
            
            <div class="menuAdmin">
                 <?php  
                 
                        if(isset($_GET['p']) && $_GET['p'] == 'showTree' || !isset($_GET['p'])){                                
                           echo $db->get_list(0);
                        }                                               
                      
                        if(isset($_GET['p']) && $_GET['p'] == 'editTree' && !isset($_GET['id'])){
                           echo $db->get_list(0);                          
                        }
                        
                        if(isset($_GET['id'])){
                                $id = $_GET['id'];
                                $where = 'id = '.$id;
                                $editItem = mysql_fetch_assoc($db->select($table,$fields=array('id','parent','name'), $where));                                   
                                $menuName = $editItem['name'];
                                $parent = $editItem['parent'];
                                $menuId = $editItem['id'];
                        }else{
                               $menuName = '';
                               $parent = '';
                               $menuId='';
                        }
                       
                        if(isset($_GET['p']) && $_GET['p'] == 'addMenu' || isset($_GET['p']) && $_GET['p'] == 'editTree' && isset($_GET['id'])){  ?>
                    
                        <form action="index.php" method="post">
                             
                            <div class="menuItems">
                                    <label  for="menuName">Menu Item name</label><input type="text" id="menuName" name="menuName" value ="<?=$menuName;?>"/>
                                    <input type="hidden" id="menuId" name="menuId" value ="<?=$menuId;?>"/>
                            </div>
                            <div class="menuItems">                      
                                   <input type="checkbox" id="enableParent" name="enableParent" /> <label  for="enableParent">Select Parent</label>
                                    <select id="selectParent" name="parent" disabled="disabled">    
                                            <?php 
                                          
                                           echo $db->get_options();
                                           
                                            if(isset($_GET['p']) && $_GET['p'] == 'addMenu'){
                                                    $sendValue = 'Add New Menu Item';
                                                    $sendName = 'addMenuItem';
                                            }
                                            if(isset($_GET['id'])){
                                                    $sendValue = 'Edit Menu Item';
                                                    $sendName = 'editMenuItem';
                                            }
                                            ?>
                                    </select>
                            </div>
                            <div class="menuSend">
                                <input type="submit" value="<?=$sendValue;?>" name="<?=$sendName;?>"/>
                            </div>

                        </form>
                
                <?php }?>
                    
            </div>
        </div>
    </body>
</html>
